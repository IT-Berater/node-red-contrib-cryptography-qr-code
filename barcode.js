module.exports = function (RED) {
  const NodeName = 'cryptoqrcode'

  function CryptoqrcodeNode (config) {
    RED.nodes.createNode(this, config)
    this.filename = config.filename
    this.prefix = config.prefix
    const node = this

    this.status({ fill: 'green', shape: 'dot', text: 'create qr-code File: ' + node.filename })

    const QRCode = require('qrcode')

    function createQRCode (bitcoinAdresse) {
      QRCode.toFile(node.filename, node.prefix + bitcoinAdresse, {
      }, function (err) {
        if (err) throw err
      })
      return 'File: ' + node.filename + ' Prefix: ' + node.prefix + ' Bitcoin Address: ' + bitcoinAdresse
    }

    this.on('input', function (msg) {
      msg.payload = createQRCode(msg.payload)
      node.send(msg)
    })
  }
  RED.nodes.registerType(NodeName, CryptoqrcodeNode)
}
