# node-red-contrib-cryptography-qr-code

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

Make from a bitcoin address a QR-Codes with NodeRed Node. Simple use.

The word “QR Code” is registered trademark of DENSO WAVE INCORPORATED in Japan and other countries.

Parameter: Filename= a name with path for the qr-code result, Prefix= a prefix for the Address from bitcoin is the prefix "bitcoin:" if the prefix is empty, you can generate other qr-codes e.g. URLs http://www.wenzlaff.info or other.

Create QR-Code like this:

![image](http://blog.wenzlaff.de/wp-content/uploads/2021/03/wenzlaff-address-qr-code.png)

# Install

Install from PALETTE Manager in NodeRed or run the following command in your NODE-RED user directory typically: ~/.node-red

npm install node-red-contrib-cryptography-qr-code

# Flow Sample


![image](http://blog.wenzlaff.de/wp-content/uploads/2021/03/wenzlaff.de-2021-03-07-um-09.50.25.png)

`[
    {
        "id": "fa6c893a.ae8d8",
        "type": "tab",
        "label": "QR-Code",
        "disabled": false,
        "info": ""
    },
    {
        "id": "15f97750.9f15d1",
        "type": "inject",
        "z": "fa6c893a.ae8d8",
        "name": "Start",
        "props": [
            {
                "p": "payload"
            },
            {
                "p": "topic",
                "vt": "str"
            }
        ],
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "topic": "1ESQwjgZa6ALM5Sha1nDZ2bU5wcQFzNFRH",
        "payload": "www.wenzlaff.de",
        "payloadType": "str",
        "x": 230,
        "y": 220,
        "wires": [
            [
                "c36f4049.b9248"
            ]
        ]
    },
    {
        "id": "4d0b1987.f7a86",
        "type": "debug",
        "z": "fa6c893a.ae8d8",
        "name": "Ergebnis",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "payload",
        "targetType": "msg",
        "statusVal": "",
        "statusType": "auto",
        "x": 860,
        "y": 220,
        "wires": []
    },
    {
        "id": "ed289c05.b34a7",
        "type": "comment",
        "z": "fa6c893a.ae8d8",
        "name": "(c) 2021 Thomas Wenzlaff www.wenzlaff.info",
        "info": "",
        "x": 770,
        "y": 140,
        "wires": []
    },
    {
        "id": "987d0461.3a3738",
        "type": "comment",
        "z": "fa6c893a.ae8d8",
        "name": "Create simple QR-Code from Bitcoin Address or other text",
        "info": "",
        "x": 370,
        "y": 140,
        "wires": []
    },
    {
        "id": "c36f4049.b9248",
        "type": "cryptoqrcode",
        "z": "fa6c893a.ae8d8",
        "name": "",
        "filename": "/data/wenzlaff-address-qr-code.png",
        "prefix": "",
        "x": 510,
        "y": 220,
        "wires": [
            [
                "4d0b1987.f7a86"
            ]
        ]
    }
]`

